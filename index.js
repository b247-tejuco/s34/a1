// [INSTALLING EXPRESS.JS FRAMEWORK PACKAGE]
// npm init enter enter enter yes
// npm install express
// create .gitignore inside folder
// put the name of files/folders u want to exclude in 'git add .'



// ---
// 1. Import express using require directive

const express= require("express")
// 2. Initialize express by using it to the app variable as a function

const app= express()

// 3. Set the port number
const port= 4004;

// 4. Use middleware to allow express to read JSON
app.use(express.json())

// 5. Use this middleware to allow express to be able to read more data types from a response
app.use(express.urlencoded({extended: true}))

// 6. Listen to the port and console.log a text once a server is running
app.listen(port, ()=> console.log(`Server is running at localhost:${port}`))

// [SECTION] Routes
	// Express has methods corresponding to each HTTP method

app.get("/hello", (request, response)=> {
	response.send('hello from the /hello endpoint')
})

// npm install -g nodemon
// nodemon index.js

app.post("/display-name", (request,response)=>{
	response.send(`Hello there ${request.body.firstName} ${request.body.lastName}!`)
})

// SUB SECTION- Signup Form
let users=[];
app.post("/signup", (request, response)=> {
	if(request.body.username !== "" && request.body.password !== ""){
		users.push(request.body);
		response.send(`User ${request.body.username} successfully registered!`)
	} else {
		response.send("Please input BOTH useranme and password")
	}
})

app.put("/changepassword", (request, response)=>{
	// creates a variable to store the message to be sent back to the client/Postman
	let message;

	// creates a for loop that will loop through the elements of the "users" array
	for(let i=0; i<users.length; i++){


		if(request.body.username == users[i].username){
			// changes the password of the user found by the loop into the password provided in the client/postman
			users[i].password = request.password;

			message= `User ${request.body.username}'s password has been updated.`


			// breaks out of loop once a user that matches  the username provided in the client/postman is found
			break;

		} else {
			message = "User does not exist!";
		}
	}
	/*send a response back to the client/Postman once the password has been updated or if a user is not found*/
	response.send(message)
})

app.get("/home", (request, response)=> {
	response.send('Welcome to the home page')
})

app.get("/users", (request, response)=> {
	


	response.send(users)
})

app.delete("/delete-user", (request, response)=>{
	response.send(`User ${request.body.username} has been deleted.`)
})
